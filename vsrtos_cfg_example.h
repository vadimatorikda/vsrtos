#ifndef VSRTOS_CFG_H
#define VSRTOS_CFG_H

/*!
 * You most must this file in your project and rename to vsrtos_cfg.h
 */

/*!
 * \brief CPU frequency in Hz.
 */
#define OC_CPU_FREQ         48000000

/*!
 * \brief Number of interrupt in second
 * for scheduler.
 */
#define OS_TICK_FREQ        10000

#endif // VSRTOS_CFG_H

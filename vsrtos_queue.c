#include "vsrtos.h"
#include "vsrtos_int_func.h"

#include <string.h>

extern volatile const vsrtos_cfg_t *c;

bool is_there_item (const queue_t *q) {
    return q->ram->items_number != 0;
}

void receive (const queue_t *q, void *item) {
    uint8_t *start_array = q->items_array;
    const void *src = &start_array[q->ram->first_item*q->item_size];
    memcpy(item, src, q->item_size);

    q->ram->first_item++;
    if (q->ram->first_item == q->items_number) {
        q->ram->first_item = 0;
    }

    q->ram->items_number--;
}

bool vsrtos_queue_receive (const queue_t *q, void *item, uint32_t timeout) {
    vsrtos_critical_start();
    if (is_there_item(q)) {
        receive(q, item);
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    if (timeout == NOT_WAIT) {
        vsrtos_critical_stop();
        return false;
    }

    // Blocking thread.
    thread_ram_t *th_st = c->threads[c->ram->thread].ram;
    th_st->state = THREAD_STATE_SUSPEND_QUEUE_RECEIVE;
    if (timeout == ALWAYS_WAIT) {
        th_st->unblock_time = timeout;
    } else {
        th_st->unblock_time = c->ram->time + timeout;
    }
    th_st->block_obj.queue = q;
    vsrtos_critical_stop();

    call_pendsv();

    // Repeat receive.
    vsrtos_critical_start();
    if (is_there_item(q)) {
        receive(q, item);
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    // Timeout.
    vsrtos_critical_stop();
    return false;
}

bool is_there_empty_call (const queue_t *q) {
    return q->ram->items_number < q->items_number;
}

static void send (const queue_t *q, const void *item) {
    uint8_t *start_array = q->items_array;
    void *dst = &start_array[q->ram->last_item*q->item_size];
    memcpy(dst, item, q->item_size);

    q->ram->last_item++;
    if (q->ram->last_item == q->items_number) {
        q->ram->last_item = 0;
    }

    q->ram->items_number++;
}

bool vsrtos_queue_send (const queue_t *q, const void *item, uint32_t timeout) {
    vsrtos_critical_start();
    if (is_there_empty_call(q)) {
        send(q, item);
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    if (timeout == NOT_WAIT) {
        vsrtos_critical_stop();
        return false;
    }

    // Blocking thread.
    thread_ram_t *th_st = c->threads[c->ram->thread].ram;
    th_st->state = THREAD_STATE_SUSPEND_QUEUE_SEND;
    if (timeout == ALWAYS_WAIT) {
        th_st->unblock_time = timeout;
    } else {
        th_st->unblock_time = c->ram->time + timeout;
    }
    th_st->block_obj.queue = q;
    vsrtos_critical_stop();

    call_pendsv();

    // Repeat receive.
    vsrtos_critical_start();
    if (is_there_item(q)) {
        send(q, item);
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    // Timeout.
    vsrtos_critical_stop();
    return false;
}

bool vsrtos_queue_send_from_isr (const queue_t *q, const void *item) {
    if (!is_there_empty_call(q)) {
        return false;
    }

    send(q, item);
    call_pendsv();
    return true;
}

uint8_t vsrtos_queue_get_item_number (const queue_t *q) {
    return q->ram->items_number;
}

void vsrtos_queue_reset (const queue_t *q) {
    vsrtos_critical_start();
    q->ram->first_item = 0;
    q->ram->last_item = 0;
    q->ram->items_number = 0;
    vsrtos_critical_stop();
    call_pendsv();
}
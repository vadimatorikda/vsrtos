#ifndef VSRTOS_MACRO_H
#define VSRTOS_MACRO_H

#ifdef __cplusplus
extern "C" {
#endif


#include "vsrtos_cfg.h"

/*!
 * \brief Macro for setting heap and len of heap in
 * object of thread_cfg_t struct.
 */
#define SET_THREAD_HEAP(x) .heap = x, .w_len = sizeof(x)/sizeof(x[0]),

/*!
 * \brief Macro for setting array of threads and
 * number of threads in object of vsrtos_cfg_t struct.
 */
#define SET_THREADS(x) .threads = x, .threads_num = sizeof(x) / sizeof(x[0])

/*!
 * \brief Macro for converting millisecond value to
 * tick value for delay or period.
 */
#define MS_TO_TICK(x) (x*(OS_TICK_FREQ/1000))

/*!
 * \brief System value for setting timeout as 'not wait'.
 */
#define NOT_WAIT 0

/*!
 * \brief System value for setting timeout as 'always wait'.
 */
#define ALWAYS_WAIT 0xFFFFFFFF

#ifdef __cplusplus
}
#endif

#endif // VSRTOS_MACRO_H

#ifndef VSRTOS_ENUMS_H
#define VSRTOS_ENUMS_H

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief It is list of all states of thread in vsrtos.
 */
typedef enum {
    /*!
     * \brief Default state after start scheduler.
     */
    THREAD_STATE_START_SUSPEND = 0,

    /*!
     * \brief Thread is running.
     */
    THREAD_STATE_RUN = 1,

    /*!
     * \brief Thread was suspended for waiting of
     * expiry delay time.
     */
    THREAD_STATE_SUSPEND_DELAY = 2,

    /*!
     * \brief Thread was suspended for waiting of
     * expiry timeout for locking of mutex.
     */
    THREAD_STATE_SUSPEND_MUTEX = 3,

    /*!
     * \brief Thread was suspended for waiting of
     * expiry timeout for taking of semaphore.
     */
    THREAD_STATE_SUSPEND_SEMAPHORE = 4,

    /*!
     * \brief Thread was suspended for waiting of
     * expiry timeout for receiving item of queue.
     */
    THREAD_STATE_SUSPEND_QUEUE_RECEIVE = 5,

    /*!
     * \brief Thread was suspended for waiting of
     * expiry timeout for sending item to queue.
     */
    THREAD_STATE_SUSPEND_QUEUE_SEND = 6,
} THREAD_STATE;

#ifdef __cplusplus
}
#endif

#endif // VSRTOS_ENUMS_H

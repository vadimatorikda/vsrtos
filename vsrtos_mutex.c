#include "vsrtos.h"
#include "vsrtos_int_func.h"

extern volatile const vsrtos_cfg_t *c;

static bool try_to_lock (mutex_t *m) {
    if (!m->locked) {
        m->locked = true;
        return true;
    }

    return false;
}

bool vsrtos_mutex_lock (mutex_t *m, uint32_t timeout) {
    vsrtos_critical_start();
    if (try_to_lock(m)) {
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    if (timeout == NOT_WAIT) {
        vsrtos_critical_stop();
        return false;
    }

    // Blocking thread.
    thread_ram_t *th_st = c->threads[c->ram->thread].ram;
    th_st->state = THREAD_STATE_SUSPEND_MUTEX;
    if (timeout == ALWAYS_WAIT) {
        th_st->unblock_time = timeout;
    } else {
        th_st->unblock_time = c->ram->time + timeout;
    }
    th_st->block_obj.mutex = m;
    vsrtos_critical_stop();

    call_pendsv();

    // Repeat receive.
    vsrtos_critical_start();
    if (try_to_lock(m)) {
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    // Timeout.
    vsrtos_critical_stop();
    return false;
}

void vsrtos_mutex_unlock (mutex_t *m) {
    vsrtos_critical_start();
    m->locked = false;
    vsrtos_critical_stop();
    call_pendsv();
}

void vsrtos_mutex_unlock_from_isr (mutex_t *m) {
    m->locked = false;
    call_pendsv();
}
#ifndef VSRTOS_H
#define VSRTOS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "vsrtos_struct.h"
#include "vsrtos_macro.h"

/*!
 * \brief Start scheduler. This function is no return.
 * It is configuring those modules automatically:
 * NVIC, SysTick.
 * It internally catch SVC, SysTick and PendSV interrupts.
 * \param[in] cfg - VSRTOS configuration struct.
 * User must create it const and write all field of this struct.
 */
void vsrtos_start_scheduler (const vsrtos_cfg_t *cfg);

/*!
 * \brief Off all interrupt. It can use only in thread.
 */
void vsrtos_critical_start ();

/*!
 * \brief On all interrupt. It can use only in thread.
 */
void vsrtos_critical_stop ();

/*!
 * \brief Switch thread.
 */
void vsrtos_yield ();

/*!
 * \brief Get RTOS time.
 * @return RTOS time.
 */
uint32_t vsrtos_get_time ();

/*!
 * \brief Delay until timestamp.
 * @param last_time - Last time stamp.
 * You can get it using vsrtos_get_time.
 * @param period - Number of RTOS ticks until continue code
 * of current thread.
 */
void vsrtos_delay_until (uint32_t *last_time, uint32_t period);

/*!
 * \brief Delay from current timestamp to current
 * time stamp plus required period.
 * @param period - Number of RTOS ticks until continue code
 * of current thread.
 * You can use macroses MS_TO_TICK for set this parameter.
 */
void vsrtos_delay (uint32_t period);

/*!
 * \brief Try to lock mutex in thread until timeout will expired.
 * @param m - Mutex.
 * @param timeout - Timeout for try to lock.
 * You can use macroses MS_TO_TICK, NOT_WAIT and ALWAYS_WAIT
 * for set this parameter.
 * @return Result of locking mutex.
 */
bool vsrtos_mutex_lock (mutex_t *m, uint32_t timeout);

/*!
 * \brief Unlock mutex in thread.
 * @param m - Mutex.
 */
void vsrtos_mutex_unlock (mutex_t *m);

/*!
 * \brief Unlock mutex from interrupt.
 * @param m - Mutex.
 */
void vsrtos_mutex_unlock_from_isr (mutex_t *m);

/*!
 * \brief Try to take mutex in thread until timeout will expired.
 * @param s - Semaphore.
 * @param timeout - Timeout for try to take.
 * You can use macroses MS_TO_TICK, NOT_WAIT and ALWAYS_WAIT
 * for set this parameter.
 * @return Result of taking mutex.
 */
bool vsrtos_semaphore_take (semaphore_t *s, uint32_t timeout);

/*!
 * \brief Give semaphore in thread.
 * @param s - Semaphore.
 */
void vsrtos_semaphore_give (semaphore_t *s);

/*!
 * \brief Give semaphore from interrupt.
 * @param s - Semaphore.
 */
void vsrtos_semaphore_give_from_isr (semaphore_t *s);

/*!
 * \brief Try to receive item from queue in thread until timeout will expired.
 * @param q - Queue.
 * @param item - Buffer for item.
 * @param timeout - Timeout for try to take.
 * You can use macroses MS_TO_TICK, NOT_WAIT and ALWAYS_WAIT
 * for set this parameter.
 * @return Result of receiving item from queue.
 */
bool vsrtos_queue_receive (const queue_t *q, void *item, uint32_t timeout);

/*!
 * \brief Try to send item to queue in thread until timeout will expired.
 * @param q - Queue.
 * @param item - Buffer with item.
 * @param timeout - Timeout for try to take.
 * You can use macroses MS_TO_TICK, NOT_WAIT and ALWAYS_WAIT
 * for set this parameter.
 * @return Result of sending item to queue.
 */
bool vsrtos_queue_send (const queue_t *q, const void *item, uint32_t timeout);

/*!
 * \brief Try to send item to queue from interrupt.
 * @param q - Queue.
 * @param item - Buffer with item.
 * @return Result of sending item to queue.
 */
bool vsrtos_queue_send_from_isr (const queue_t *q, const void *item);

/*!
 * \brief Returns number of items in queue.
 * @param q - Queue.
 * @return Number of items in queue.
 */
uint8_t vsrtos_queue_get_item_number (const queue_t *q);

/*!
 * \brief Reset queue state.
 * @param q - queue.
 */
void vsrtos_queue_reset (const queue_t *q);

#ifdef __cplusplus
}
#endif

#endif // VSRTOS_H

# vsrtos

Very simple real time operating system is operating system that take a little resources.

It supported only Cortex-M0 core.

It can:
1. Mutex, semaphore, queue operations.
2. Thread in cooperative mode.
3. Critical sections.

There is more [descriptions](https://habr.com/ru/post/570648/).
You can see example using in that projects:
1. [lib_service](https://gitlab.com/vadimatorikda/lib_service) - service functions without hardware dependencies.
2. [w7500p_test](https://gitlab.com/vadimatorikda/w7500p_test) - example project. It is sending UDP packages form 
W7500p board to PC.
#include "vsrtos.h"
#include "vsrtos_int_func.h"

extern volatile const vsrtos_cfg_t *c;

static bool try_to_take (semaphore_t *s) {
    if (s->in_stock) {
        s->in_stock = false;
        return true;
    }

    return false;
}

bool vsrtos_semaphore_take (semaphore_t *s, uint32_t timeout) {
    vsrtos_critical_start();
    if (try_to_take(s)) {
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    if (timeout == NOT_WAIT) {
        vsrtos_critical_stop();
        return false;
    }

    // Blocking thread.
    thread_ram_t *th_st = c->threads[c->ram->thread].ram;
    th_st->state = THREAD_STATE_SUSPEND_SEMAPHORE;
    if (timeout == ALWAYS_WAIT) {
        th_st->unblock_time = timeout;
    } else {
        th_st->unblock_time = c->ram->time + timeout;
    }
    th_st->block_obj.semaphore = s;
    vsrtos_critical_stop();

    call_pendsv();

    // Repeat receive.
    vsrtos_critical_start();
    if (try_to_take(s)) {
        vsrtos_critical_stop();
        call_pendsv();
        return true;
    }

    // Timeout.
    vsrtos_critical_stop();
    return false;
}

void vsrtos_semaphore_give (semaphore_t *s) {
    vsrtos_critical_start();
    s->in_stock = true;
    vsrtos_critical_stop();
    call_pendsv();
}

void vsrtos_semaphore_give_from_isr (semaphore_t *s) {
    s->in_stock = true;
    call_pendsv();
}

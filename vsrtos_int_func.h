#ifndef VSRTOS_INT_FUNC_H
#define VSRTOS_INT_FUNC_H

#include "cortex_m0_description.h"

/*!
 * \brief Call pendsv interrupt with protect
 * of data and instruction.
 */
static inline __attribute__((always_inline))
void call_pendsv () {
    SCB->ICSR = 1 << 28;
    __asm volatile("dsb    \n"
                   "isb    \n");
}


#endif // VSRTOS_INT_FUNC_H

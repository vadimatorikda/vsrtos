#include "vsrtos.h"
#include "cortex_m0_description.h"
#include "vsrtos_int_func.h"

#include <stddef.h>

volatile const vsrtos_cfg_t *c = NULL;

static inline __attribute__((always_inline))
void check_cfg (const vsrtos_cfg_t *cfg) {
    if (cfg->ram->started) {while (true) {}}    // Check that OS is not started.
    if (cfg->threads_num < 2) {while (true) {}}   // Check threads number.
    if (c != NULL) {while (true) {}}              // Check old start.
}

static inline __attribute__((always_inline))
void init_systick () {
    uint32_t start_value = (OC_CPU_FREQ/SYSTIC_DIV)/OS_TICK_FREQ - 1;
    SYSTIC->RVR = start_value;
    SYSTIC->CVR = start_value;
    SYSTIC->CSR = (1 << 2) | (1 << 1) | (1 << 0);   // Counter enabled + Exception request.
}

static inline __attribute__((always_inline))
void init_irq () {
    SCB->SHPR3 = (0x00 << 24) | (0xF0 << 16);       // PendSV - lowest. SysTick - highest.
}

#define STACK_PROTECT_VALUE 0xA3A33A3A

static inline __attribute__((always_inline))
void init_thread_stack_protection (uint32_t *heap) {
    heap[0] = STACK_PROTECT_VALUE;
    heap[1] = STACK_PROTECT_VALUE;
    heap[2] = STACK_PROTECT_VALUE;
    heap[3] = STACK_PROTECT_VALUE;
}

#define IRQ_AUTO_SAVE_REG_NUM 8

static inline __attribute__((always_inline))
void init_thread_stack_pointer (const thread_cfg_t *t_st) {
    t_st->ram->sp = (uint32_t)(t_st->heap + t_st->w_len - IRQ_AUTO_SAVE_REG_NUM);
}

// Return to Thread mode.
// Exception return gets ram from PSP.
// Execution uses PSP after return.
#define EXC_RETURN 0xFFFFFFFD

typedef struct __attribute__ ((packed)) {
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t x_psr;
} sp_auto_save_regs;

static inline __attribute__((always_inline))
void init_thread_registers (const thread_cfg_t *t_st) {
    sp_auto_save_regs *regs = (sp_auto_save_regs *)(t_st->ram->sp);

    // Auto-save registers.
    regs->pc = (uint32_t)t_st->func;
    regs->lr = EXC_RETURN;
    regs->x_psr = (1 << 24);
    regs->r0 = (uint32_t)t_st->obj;
}

static inline __attribute__((always_inline))
void init_threads (const vsrtos_cfg_t *cfg) {
    for (uint32_t i = 0; i < cfg->threads_num; i++) {
        const thread_cfg_t *t_st = &cfg->threads[i];
        init_thread_stack_protection(t_st->heap);
        init_thread_stack_pointer(t_st);
        init_thread_registers(t_st);
    }
}

static inline __attribute__((always_inline))
void set_psp_stack () {
    uint32_t control = (1 << 1);
    __asm volatile ("MSR control, %0" : : "r" (control) : "memory");
    __asm volatile ("isb");
}

_Noreturn void vsrtos_start_scheduler (const vsrtos_cfg_t *cfg) {
    check_cfg(cfg);
    c = cfg;                    // All methods use global pointer.
    init_threads(cfg);
    init_irq();
    init_systick();
    __asm volatile("SVC 0");
    while (true);                // Exit protection (error).
}

static inline __attribute__((always_inline))
void load_psp_of_new_thread (uint32_t thread_psp) {
    __asm volatile ("MSR psp, %0" : : "r" (thread_psp) : );
}

#define IDLE_THREAD_NUM 0

void SVC_Handler () {
    c->ram->thread = IDLE_THREAD_NUM + 1;
    c->threads[c->ram->thread].ram->state = THREAD_STATE_RUN;
    load_psp_of_new_thread(c->threads[c->ram->thread].ram->sp);
    set_psp_stack();
    c->ram->started = true;
    __asm volatile("cpsie i");
}

static inline __attribute__((always_inline))
void check_delay_blocked () {
    for (uint32_t i = 1; i < c->threads_num; i++) {
        thread_ram_t *th_state = c->threads[i].ram;
        switch (th_state->state) {
            case THREAD_STATE_SUSPEND_DELAY:
            case THREAD_STATE_SUSPEND_MUTEX:
            case THREAD_STATE_SUSPEND_SEMAPHORE:
            case THREAD_STATE_SUSPEND_QUEUE_RECEIVE:
            case THREAD_STATE_SUSPEND_QUEUE_SEND:
                if (th_state->unblock_time != ALWAYS_WAIT) {
                    if (th_state->unblock_time <= c->ram->time) {
                        th_state->state = THREAD_STATE_START_SUSPEND;
                    }
                }
                break;

            default:
                break;
        }
    }
}

void SysTick_Handler () {
    c->ram->time++;

    check_delay_blocked();

    call_pendsv();
}

static inline __attribute__((always_inline))
void suspend_current_thread () {
    const thread_cfg_t *cur_th = &c->threads[c->ram->thread];
    if (cur_th->ram->state == THREAD_STATE_RUN) {
        cur_th->ram->state = THREAD_STATE_START_SUSPEND;
    }
}

static inline __attribute__((always_inline))
uint32_t find_next_thread () {
    uint32_t th_num = IDLE_THREAD_NUM;
    for (uint32_t i = 1; i < c->threads_num; i++) {
        thread_ram_t *th_state = c->threads[i].ram;

        switch (th_state->state) {
            case THREAD_STATE_START_SUSPEND:
                th_num = i;
                break;

            case THREAD_STATE_SUSPEND_MUTEX:
                if (!th_state->block_obj.mutex->locked) {
                    th_num = i;
                }
                break;

            case THREAD_STATE_SUSPEND_SEMAPHORE:
                if (th_state->block_obj.semaphore->in_stock) {
                    th_num = i;
                }
                break;

            case THREAD_STATE_SUSPEND_QUEUE_SEND:
                if (th_state->block_obj.queue->ram->items_number != th_state->block_obj.queue->items_number) {
                    th_num = i;
                }
                break;

            case THREAD_STATE_SUSPEND_QUEUE_RECEIVE:
                if (th_state->block_obj.queue->ram->items_number != 0) {
                    th_num = i;
                }
                break;

            default:
                break;
        }
    }

    return th_num;
}

static inline __attribute__((always_inline))
void save_psp_of_current_thread (uint32_t psp) {
    c->threads[c->ram->thread].ram->sp = psp;
}

static inline __attribute__((always_inline))
void check_current_thread_stack () {
    const thread_cfg_t *cur_th = &c->threads[c->ram->thread];
    if ((cur_th->heap[0] != STACK_PROTECT_VALUE) |
        (cur_th->heap[1] != STACK_PROTECT_VALUE) |
        (cur_th->heap[2] != STACK_PROTECT_VALUE) |
        (cur_th->heap[3] != STACK_PROTECT_VALUE)) {while (true) {}}
}


uint32_t pend_sv_handler (uint32_t psp) {
    save_psp_of_current_thread(psp);
    suspend_current_thread();
    check_current_thread_stack();
    uint32_t next_thread = find_next_thread();

    c->ram->thread = next_thread;
    c->threads[next_thread].ram->state = THREAD_STATE_RUN;
    return c->threads[next_thread].ram->sp;
}

__attribute__((naked))
void PendSV_Handler () {
    __asm volatile
    (
    "    .syntax unified              \n"
    // Load PSP.
    "    mrs r0, psp                  \n"
    // Save to stack additional registers [R4-R11],
    "    subs r0, r0, #32             \n"
    "    stmia r0!, {r4-r7}           \n"
    "    mov r4, r8                   \n"
    "    mov r5, r9                   \n"
    "    mov r6, r10                  \n"
    "    mov r7, r11                  \n"
    "    stmia r0!, {r4-r7}           \n"
    // Switch thread.
    "    cpsid i                      \n"
    "    bl pend_sv_handler           \n"
    "    cpsie i                      \n"
    // Save PSP.
    "    msr psp, r0                  \n"
    // Set LR.
    "    ldr r1, =0xFFFFFFFD          \n"
    "    mov lr, r1                   \n"
    // Load [R4-R11].
    "    subs r0, r0, #16             \n"
    "    ldmia r0!, {r4-r7}           \n"
    "    mov r8, r4                   \n"
    "    mov r9, r5                   \n"
    "    mov r10, r6                  \n"
    "    mov r11, r7                  \n"
    "    subs r0, r0, #32             \n"
    "    ldmia r0!, {r4-r7}           \n"

    "    bx lr                        \n"
    );
}

void vsrtos_critical_start () {
    __asm volatile("cpsid i");
    c->ram->critical_counter++;
}

void vsrtos_critical_stop () {
    if (c->ram->critical_counter) {
        c->ram->critical_counter--;
    }

    if (!c->ram->critical_counter) {
        __asm volatile("cpsie i");
    }
}

uint32_t vsrtos_get_time () {
    return (c != NULL)?c->ram->time:0;
}

void vsrtos_delay_until (uint32_t *last_time, uint32_t period) {
    if (c == NULL) {while (true) {}}
    if (period == 0) {while (true) {}}
    c->threads[c->ram->thread].ram->state = THREAD_STATE_SUSPEND_DELAY;
    c->threads[c->ram->thread].ram->unblock_time = *last_time + period;
    *last_time += period;
    call_pendsv();
}

void vsrtos_delay (uint32_t period) {
    if (c == NULL) {while (true) {}}
    if (period == 0) {while (true) {}}
    c->threads[c->ram->thread].ram->state = THREAD_STATE_SUSPEND_DELAY;
    c->threads[c->ram->thread].ram->unblock_time = c->ram->time + period;
    call_pendsv();
}

void vsrtos_yield () {
    call_pendsv();
}
#ifndef CORE_H
#define CORE_H

#include <stdint.h>

#include "vsrtos_cfg.h"

/*!
 * \brief Division CPU clock for systic.
 */
#ifndef SYSTIC_DIV
#define SYSTIC_DIV 8
#endif

/*!
 * \brief Hardware registers for Systic module.
 */
typedef struct {
    volatile uint32_t CSR;
    volatile uint32_t RVR;
    volatile uint32_t CVR;
    volatile uint32_t CALIB;
} systic_struct;

/*!
 * \brief Hardware registers for SCB module.
 */
typedef struct {
    volatile uint32_t CPUID;
    volatile uint32_t ICSR;
    volatile uint32_t RES_1;
    volatile uint32_t AIRCR;
    volatile uint32_t SCR;
    volatile uint32_t CCR;
    volatile uint32_t RES_2;
    volatile uint32_t SHPR2;
    volatile uint32_t SHPR3;
} scb_struct;

#define SYSTIC    ((systic_struct *)       0xE000E010)
#define SCB       ((scb_struct *)          0xE000ED00)

#endif // CORE_H

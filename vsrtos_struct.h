#ifndef VSRTOS_STRUCTS_H
#define VSRTOS_STRUCTS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#include "vsrtos_enum.h"

/*!
 * \brief Object of this structure contains state of mutex.
 * Object must be initialized of zero value.
 * Object must be volatile.
 */
typedef struct {
    /*!
     * \brief State of mutex: true - locked, false - unlocked.
     */
    bool locked;
} mutex_t;

/*!
 * \brief Object of this structure contains state of semaphore.
 * Object must be initialized of zero value.
 * Object must be volatile.
 */
typedef struct {
    /*!
     * \brief State of semaphore: true - in_stock, false - not available.
     */
    bool in_stock;
} semaphore_t;

/*!
 * \brief Object of this structure contains volatile info of queue.
 * Object must be initialized of zero value.
 * Object must be volatile.
 */
typedef struct {
    /*!
     * \brief Pointer to start item in ring buffer.
     */
    uint8_t first_item;

    /*!
     * \brief Pointer to end item in ring buffer.
     */
    uint8_t last_item;

    /*!
     * \brief Number of items in ring buffer.
     */
    uint8_t items_number;
} queue_ram_t;

/*!
 * \brief Object of this structure contains configuration of queue.
 * Object must be filled by user.
 * Object must be non-volatile.
 */
typedef struct {
    /*!
     * \brief It is volatile array. Size of each item is item_size.
     * Size of array if item_size * items_number.
     */
    void *items_array;

    /*!
     * \brief Size of one item in array in bytes.
     */
    uint8_t item_size;

    /*!
     * \brief Number of items in array.
     */
    uint8_t items_number;

    /*!
     * \brief Volatile data for work of queue.
     */
    queue_ram_t *ram;
} queue_t;

/*!
 * \brief Object of this structure contains volatile data
 * of object of thread_cfg_t structure.
 * Object must be initialized of zero value.
 * Object must be volatile.
 */
typedef struct {
    /*!
     * \brief Current state of thread.
     */
    THREAD_STATE state;

    /*!
     * \brief Stack pointer of thread.
     */
    uint32_t sp;

    /*!
     * \brief Timestamp for unblock thread.
     */
    uint32_t unblock_time;

    /*!
     * \brief Object which is blocking thread.
     */
    union {
        /*!
         * \brief Mutex object.
         */
        mutex_t *mutex;

        /*!
         * \brief Semaphore object.
         */
        semaphore_t *semaphore;

        /*!
         * \brief Queue object.
         */
        const queue_t *queue;
    } block_obj;
} thread_ram_t;

/*!
 * \brief Object of this structure contains volatile data
 * of object of vsrtos_cfg_t structure.
 * Object must be initialized of zero value.
 * Object must be volatile.
 */
typedef struct {
    /*!
     * \brief Scheduling is started.
     */
    bool started;

    /*!
     * \brief Current value of system tick counter.
     */
    uint32_t time;

    /*!
     * \brief Current number of running task.
     */
    uint32_t thread;

    /*!
     * \brief Critical section entering counter.
     */
    uint32_t critical_counter;
} vsrtos_ram_t;

/*!
 * \brief Object of this structure contains configuration of thread.
 * Object must be filled by user.
 * Object must be non-volatile.
 * User can use macro SET_THREAD_HEAP for init those fields:
 * heap and w_len
 */
typedef struct {
    /*!
     * \brief Function of thread.
     * @param obj - object for thread
     */
    void (*func) (const void *obj);

    /*!
     * \brief Object for thread function.
     */
    const void *obj;

    /*!
     * \brief Heap buffer for thread. It has w_len length.
     */
    uint32_t *heap;

    /*!
     * \brief Length of heap buffer.
     */
    uint32_t w_len;

    /*!
     * \brief Volatile data for work of thread.
     */
    thread_ram_t *ram;
} thread_cfg_t;

/*!
 * \brief Object of this structure contains configuration of vsrtos.
 * Object must be filled by user.
 * Object must be non-volatile.
 * User can use macro SET_THREADS for init those fields:
 * threads and threads_num
 */
typedef struct {
    /*!
     * \brief Array of threads configs.
     * Important: first thread must be IDLE!
     */
    const thread_cfg_t *threads;

    /*!
     * \brief Number of threads.
     */
    uint32_t threads_num;

    /*!
     * \brief Volatile data for work of vsrtos.
     */
    vsrtos_ram_t *ram;
} vsrtos_cfg_t;

#ifdef __cplusplus
}
#endif

#endif // VSRTOS_STRUCTS_H
